# High Performance Computing as a Service (HPCaaS)

This project provides a on-demand HPC as a service solution to users of the African Research Cloud who require a specialized environment. This HPCaaS is based on Elasticluster, which was developed by the talented engineers at the University of Zurich. We decided to fork our own version and work on improving it.

## Credit

Elasticluster was developed by the University of Zurich. More information can be found at http://gc3-uzh-ch.github.io/elasticluster/

## Getting Started

* Credentials for the African Research Cloud (ARC) - https://dashboard.arc.ac.za

## Pre-requisite:

* Python 2.7 - Anaconda is preferred as all site packages are self-contained - https://www.continuum.io
* Ansible 2.1

## Installation:

* Install and configure Anaconda as per the instructions.
* Clone the following repository: "git clone https://gitlab.com/africanresearchcloud/hpc.git "
* Change directory into the clone respository and execute "python setup.py install". The install will download additional python packages. You may want to run the install in a python virtenv or alternatively in your own anaconda environment. 
* Upon successfully installing Elasticluster, the binary will be made available and in your path.


## Customize Ansible Playbooks:

All playbooks are located in "<cloned_dir>elasticluster/share/playbooks/roles". Update the playbooks as you see fit but understanding the playbook execution process is fundamental. After every notebook update its important to execute the "python setup.py install" in the root folder to include your changes.

## Elasticluster 
### First Steps:  

Elasticluster creates a configuration file and a storage directory that contains a few YAML files. This resides in your home directory under the ".elasticluster" directory. 

By default, the configuration files do not exist so its important to generate the configuration files by executing the " elasticluster list-templates". The list-templates option will check to see if any configuration files exist and if not generate them.

Elasticluster configures everything a typical HPC cluster is built with. The NFS bits are the most important. The version of Elasticluster which the University of Zurich makes available exports the root disk as its /home. This has not changed but what has changed is the inclusion of an additional /data export. The data export still use the root disk, by default but at least gives you the opportunity to change to a larger or persistant volume.  


### 1. Create your OpenStack Network and Security Groups: 

First off, its important to create our private network, access and security groups for our environment. If you already have this setup, obtain the network IDs and security group IDs from your respective cloud provider CLI or GUI and complete the sections of the config file below. If however you do not have anything setup in your tenant space, find the instructions below on how to create the network and security group environment. 

Source your openrc from the command line prior to executing the commands below. 

```
source testing-openrc.sh  - Obtain the openrc.sh for your tenant from the API access table under Access and Security 
nova secgroup-create HPC-secgrp "This is a HPC security group"
nova secgroup-add-rule HPC-secgrp tcp 22 22 0.0.0.0/0
nova secgroup-add-rule HPC-secgrp icmp -1 -1 0.0.0.0/0
nova secgroup-add-rule HPC-secgrp udp 1 65535 192.168.1.0/24
nova secgroup-add-rule HPC-secgrp tcp 1 65535 192.168.1.0/24
```
### 2. Create Network:
```
neutron net-create HPC-network
neutron subnet-create HPC-network 192.168.1.0/24
```
### Obtain NetworkIDs and Security IDS:
The network IDs and security IDs you will populate into your configuration file below,  
```
nova network-show HPCtest-net
nova secgroup-list | grep -i HPCtest
```

### 3. Create a Data disk: 

As mentioned in the " First Steps " above, it might be important for you to have an additional data disk todo stuff with. You may either require it for the installation of software, a persistent disk to store data on or a scratch volume other that /home. You decide. 

You can create this disk by issuing the following command. 

```
cinder create --metadata fstype=ext4 fslabel=data dio=yes --display-name HPC_Data 10
```


### 4. Elasticluster Config: 

Example of a configuration file of spinning up a Torque/Maui CentOS HPCaaS environment: ~/.elasticluster/config

```
[cloud/arc]
provider=openstack
auth_url=https://keystone.arc.ac.za:5000/v2.0
username=REPLACE_WITH_USERNAME_ARC
password=REPLACE_WITH_PASSWORD_ARC
project_name=REPLACE_WITH_PROJECT_NAME
region_name=ZA
request_floating_ip=True

[login/centos]
image_user=centos
image_user_sudo=root
image_sudo=True
user_key_name=<Provide_a_description_of_key>
user_key_private=/location/of_your_/private_key
user_key_public=/location/of_your_/public_key

[setup/ansible-pbs]
provider=ansible
frontend_groups=pbs_master,maui_master
compute_groups=pbs_clients
ansible_ssh_pipelining=no

[cluster/torque-centos]
cloud=arc
login=centos
setup_provider=ansible-pbs
security_group=<Replace_With_Security_IDS_here>
# CentOS image_id available in the ARC
image_id=aefb815a-1aa2-468e-b64b-bed5c3f42e05
flavor=m1.medium
frontend_nodes=1
compute_nodes=2
ssh_to=frontend
# You need to create a network before hand and attach its network_id below
network_ids=<replace_with_IDs_here>
```


## Time to start a HPC Cluster:

In order to deploy a HPC cluster you will need to execute the following command:
``` 
elasticluster start torque-centos -n HPC-Cluster
```
Just to provide a bit of detail around what the above command does is it executes elasticluster and starts a new cluster with the configuration as defined in the ~/.elasticluster/config file by referencing an explicit attribute, torque-centos. The -n provides a description of the cluster. 

## Data Disk Attachment: 

In order to attach the data disk to the node you will need to obtain the instance_ID for the frontend node and the volume_ID for the volume. Execute the following to obtain instance_ID:

```
nova list | grep -i frontend 
```
.. and now for the volume_ID, 

```
cinder list | grep -i <the_name_of_your_volume>
```
.. and now lets attach the volume to the instance. 

```
nova volume-attach INSTANCE_ID VOLUME_ID auto 
```

Your volume should not be attached to the frontend node. 


The instance_ID is then used to associate the volume with the instance. 

## Resizing the cluster: 

Resizing the cluster is important and can be done in a very seemless way. 

```
elasticluster resize HPC-Cluster -a 6:compute
```
The above will resize the compute group of the cluster. By default, the configuration file is setup to deliver 1 headnode and 2 compute node instances. In the example above, the resize command will increase the cluster with 4 additional compute instances and thereby formulating the cluster with 6 compute instances. During the resize the additional nodes will be installed and configured as per the other nodes in the cluster. Please allow some time for the resize to occur. During the resize operation the cluster will continue to work.  

## Destroying the cluster:

``` 
elastic stop HPC-Cluster 
``` 

The above command will shutdown and terminate all compute and frontend instances. Disks are not persistant and so its important for you to ensure that you data is moved off the cluster before you terminate it. You can however assign additional persistent volumes to the cluster afterwards. 


---
#
# Install all "standard" software that could be needed for installing
# other software, and for effectively managing a cluster.  Basically
# this includes the following categories of utilities:
#
# - compression utilities and archivers
# - version control systems
# - standard UNIX build utilities like C compiler, make, etc.
# - a few other misc utilities that one can reasonably expect
#

- name: Install commonly needed software (Debian-family)
  when: is_debian_or_ubuntu
  package:
    name={{item}}
    state=present
  with_items:
    # compression and archivers
    - bzip2
    - cpio
    - gzip
    - lzip
    - p7zip-full
    - tar
    - unzip
    - xz-utils
    - zip
    # version control systems
    - git
    - mercurial
    - subversion
    # basic build environment
    - g++
    - gcc
    - libc6-dev
    - make
    # other "standard" utilities
    - kexec-tools
    - m4
    - moreutils
    - rsync
    - screen
    - tmux
    - vim
    

- name: Install commonly needed software (RHEL-family)
  when: is_centos
  package:
    name={{item}}
    state=present
  with_items:
    # compression and archivers
    - bzip2
    - cpio
    - gzip
    - lzip
    - p7zip
    - tar
    - unzip
    - xz
    - zip
    # version control systems
    - git
    - mercurial
    - subversion
    # basic build environment
    - gcc
    - gcc-c++
    - gcc-gfortran
    - glibc-devel
    - make
    # other "standard" utilities
    - kexec-tools
    - m4
    - moreutils
    - moreutils-parallel
    - rsync
    - screen
    - tmux
    - vim
    - psmisc.x86_64
    - mpich.x86_64
    - mpich-devel.x86_64
    - mpich-doc.noarch
    - mpitests-mpich.x86_64
    - netcdf-fortran-mpich.x86_64
    - hdf5-mpich.x86_64
    - hdf5-mpich-devel.x86_64
    - hdf5-mpich-static.x86_64

- name: Removal section of software (Particular for Elasticluster auto-provision) (RHEL-family)
  when: is_centos
  package:
    name={{item}}
    state=removed
  with_items:
     - cloud-init
     - mpi4py-mpich
     - mpi4py-common

- name: Ensure /opt/dropbox directory exists
  action: file path=/opt/dropbox state=directory

- name: Download DropBox Client
  get_url:
    url: https://www.dropbox.com/download?plat=lnx.x86_64
    dest: /tmp/dropbox-cli.tar.gz
    mode: 0440

- name: Download Cosmocalc
  get_url:
    url: http://cxc.harvard.edu/contrib/cosmocalc/downloads/cosmocalc-0.1.2.tar.gz
    dest: /tmp/cosmocalc-0.1.2.tar.gz
    mode: 0440

- name: Ensure /tmp/cosmocalc-0.1.2 directory exists
  action: file path=/tmp/cosmocalc-0.1.2 state=directory

# Unarchive Software
- unarchive: src=/tmp/dropbox-cli.tar.gz dest=/opt/dropbox copy=no
- unarchive: src=/tmp/cosmocalc-0.1.2.tar.gz dest=/tmp copy=no

- name: Additional misc. packages
  when: is_centos
  shell: "{{ item }}"
  with_items:
    - cd /tmp/cosmocalc-0.1.2 && python setup.py install
